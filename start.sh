#!/bin/bash

## MI Cloud Rest Base Server

export EUREKA_SERVER_URI=http://localhost:8761/micloudeureka
export SERVER_PORT=8089

if [ "$1" == "debug" ]; then
    mvn spring-boot:run -Drun.jvmArguments="-agentlib:jdwp=transport=dt_socket,server=y,suspend=n,address=5008" --debug
else
    mvn spring-boot:run $@
fi
