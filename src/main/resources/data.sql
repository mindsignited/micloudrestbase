-- Using this file for now as it will get executed after context is built
-- i would prefer to use liquibase, which we can get working later.

insert into device(name, model, version) values ('IP Camera', 'MontaVista', 0)
insert into device(name, model, version) values ('LTE Camera', 'MontaVista', 0)
insert into device(name, model, version) values ('Bullet Camera', 'MontaVista', 0)
insert into device(name, model, version) values ('Raspberry PI', 'Raspbian', 0)
insert into device(name, model, version) values ('Encore Networks', 'OpenWRT', 0)
