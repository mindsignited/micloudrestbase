package com.mindsignited.micloudrestbase;

import com.mindsignited.keycloak.resource.EnableKeycloakOAuth2Resource;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

/**
 *
 **/
@EnableDiscoveryClient
@SpringBootApplication
@EnableAspectJAutoProxy
@EnableKeycloakOAuth2Resource
public class WebappApplication {

    public static void main(String[] args) {
        SpringApplication.run(WebappApplication.class, args);
    }

}
