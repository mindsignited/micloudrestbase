package com.mindsignited.micloudrestbase.domain;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * Created by nicholaspadilla on 1/12/15.
 */
@Getter
@Setter
@Entity
public class Device implements Serializable {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long id;

    @Version
    private Integer version;

    @Column(nullable= false)
    @NotNull
    private String name;

    @Column(nullable= false)
    @NotNull
    private String model;
}
