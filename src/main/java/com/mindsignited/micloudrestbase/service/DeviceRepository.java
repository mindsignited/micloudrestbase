package com.mindsignited.micloudrestbase.service;

import com.mindsignited.micloudrestbase.domain.Device;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.List;

/**
 * Created by nicholaspadilla on 1/12/15.
 */
@RepositoryRestResource(collectionResourceRel = "devices", path = "devices")
interface DeviceRepository extends PagingAndSortingRepository<Device, Long> {

    Device findByNameAndModel(@Param("name") String name, @Param("model") String model);

    List<Device> findByModel(@Param("model") String model);

}